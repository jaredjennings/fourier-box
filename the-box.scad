partno = 0;

use <softcube.scad>;
use <eggcrate.scad>;

fourier_h = 34; // with dsa keycaps and these bumpons I got at HackPGH
fourier_w = 142; // this is the widest one; haven't measured the other
fourier_d = 93;
wiggle_room = 2;
fourier_size = [fourier_w + wiggle_room,
                fourier_d + wiggle_room,
                fourier_h + wiggle_room];
magnet_r = 1.5;
magnet_h = 1.0;
epoxy_room = 1.0;


min_thickness = 4; // this object is going in my backpack every day

fillet_radius = [20,20,12];
fillet_allowance = 0.3 * fillet_radius;

inner_fillet_radius = [7,7,5];

walls = [ 2 * min_thickness,
          2 * min_thickness,
          2 * min_thickness];

epsilon = 0.1;

function sumv(v,i=0) = (i==(len(v)-1) ? v[i] : v[i] + sumv(v,i+1));
function sumto(t, v, i=0) = (i==t ? 0 : v[i] + sumto(t, v, i+1));

compartment_sizes = [
     [fourier_size.x, 0.25 * fourier_size.y, 0.75*fourier_size.z],
     fourier_size,
     [fourier_size.x, 0.25 * fourier_size.y, 0.5*fourier_size.z],
     fourier_size,
     [fourier_size.x, 0.25 * fourier_size.y, 0.75*fourier_size.z],
     ];

compartment_translations = [
     [0,0, 0.25*fourier_size.z],
     [0,0,0],
     [0,0, 0.5*fourier_size.z],
     [0,0,0],
     [0,0, 0.25*fourier_size.z],
     ];

all_compartments_y = [for(vi=[0:len(compartment_sizes)-1])
          compartment_sizes[vi].y];
all_compartments_size = [fourier_size.x,
                         sumv(all_compartments_y) + (len(compartment_sizes) - 1) * min_thickness,
                         fourier_size.z];

whole_box_size = all_compartments_size + walls + fillet_allowance;
top_split_down = min_thickness + fillet_allowance.z;
top_split_up = whole_box_size.z - top_split_down;

module compartments() {
     for(i=[0:len(compartment_sizes)-1]) {
          translate(compartment_translations[i])
          translate(walls / 2 + fillet_allowance / 2)
               translate([0, sumto(i, all_compartments_y) +
                          i * min_thickness, 0])
               cube_c2_fillet(
                    compartment_sizes[i] + [0,0,0.5*inner_fillet_radius.z],
                    inner_fillet_radius,
                    inner_fillet_radius);
     }
}

module pull() {
     size = [4, 30, 4];
     fillet = [2,2,2];
     translate([whole_box_size.x - min_thickness / 2,
                whole_box_size.y / 2,
                whole_box_size.z - top_split_down])
          cube_c2_fillet(size, fillet, fillet, center=true);
}

module place_along_edge(nx, ny, inset=0, from_corner=10, y_exceptions=[]) {
     x1 = from_corner;
     x2 = whole_box_size.x - from_corner;
     y1 = from_corner;
     y2 = whole_box_size.y - from_corner;
     xu = (x2 - x1) / (nx);
     yu = (y2 - y1) / (ny);
     x_from_edge = min_thickness/2 + fillet_allowance.x/4;
     y_from_edge = min_thickness/2 + fillet_allowance.y/4;
     for(xi=[0:nx-1]) {
          translate([x1 + xu * (xi+0.5), y_from_edge, top_split_up])
               children();
          translate([x1 + xu * (xi+0.5),
                     whole_box_size.y - y_from_edge,
                     top_split_up])
               children();
     }
     for(yi=[0:ny-1]) {
          
          translate([x_from_edge, y1 + yu * (yi+0.5), top_split_up])
               children();
          if(len(search(yi, y_exceptions))==0) {
               translate([whole_box_size.x - x_from_edge,
                          y1 + yu * (yi+0.5),
                          top_split_up])
                    children();
          }
     }
}

module magnet_hole_pair() {
     // we need two magnets tall plus room for each for epoxy behind
     // and on sides
     cylinder(r=magnet_r+epoxy_room, h=2*magnet_h+2*epoxy_room,
              center=true);
}

module whole_box() {
     difference() {
          cube_c2_fillet(whole_box_size,
                         fillet_radius,
                         fillet_radius,
                         splinesteps=12);
          compartments();
          pull();
          place_along_edge(4, 9, y_exceptions=[4], from_corner=10) {
               magnet_hole_pair();
          }
     }
}

module bottom_intersect() {
     size = whole_box_size + [0,0,-top_split_down];
     slop=1;
     translate([-0.5*slop, -0.5*slop, -0.5*slop])
     cube(size+[slop,slop,slop]);
}
module top_intersect() {
     size = whole_box_size + [0,0,-top_split_down];
     slop=1;
     tolerance=0.2;
     translate([-0.5*slop, -0.5*slop, -0.5*slop + size.z + tolerance])
     cube(size+[slop,slop,slop]);
}

module nodule() { sphere(2.5); }
module dimple() { sphere(2.6); }
module nodule_places() {
     place_along_edge(3, 8, y_exceptions=[3, 4], from_corner=25)
          children();
}

module bottom() {
     union() {
          intersection() { whole_box(); bottom_intersect(); }
          nodule_places() nodule();
     }
}
module top() {
     difference() {
          intersection() { whole_box(); top_intersect(); }
          nodule_places() dimple();
     }
}

module piece_intersect(pn, lace_hole_frequency) {
     // lace hole radius is set in eggcrate.scad
     piece_gap = 0.1;
     translate([-15,0,-5])
          rotate(a=8,v=[0,1,0])
          rotate(a=-90,v=[0,0,1])
          x_space_filling_eggcrate_box(
               pn, 375, piece_gap, lace_hole_frequency, [125, 200, 70],
               [1.5,1.5,1.5], [1/14, 1/15, 1/16],
               5);
}
if(partno == -1) {
     bottom_intersect();
     top_intersect();
} else if(partno == 0) {
     bottom();
     translate([-20,0,50]) rotate(a=180,v=[0,1,0]) top();
} else if(partno <= 3) {
     intersection() {
          bottom();
          piece_intersect(partno-1, 0);
     }
} else {
     intersection() {
          translate([0, whole_box_size.y, whole_box_size.z])
               rotate(a=180, v=[1,0,0])
               top();
          piece_intersect(partno-4, 1/15);
     }
}
