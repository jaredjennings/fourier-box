include <BOSL/constants.scad>
use <BOSL/beziers.scad>
use <eggcrate.scad>

function _000(v) = [0,0,0];
function _00p(v) = [0,0,v.z];
function _00m(v) = [0,0,-v.z];
function _0p0(v) = [0,v.y,0];
function _0pp(v) = [0,v.y,v.z];
function _0pm(v) = [0,v.y,-v.z];
function _0m0(v) = [0,-v.y,0];
function _0mp(v) = [0,-v.y,v.z];
function _0mm(v) = [0,-v.y,-v.z];

function _p00(v) = [v.x,0,0];
function _p0p(v) = [v.x,0,v.z];
function _p0m(v) = [v.x,0,-v.z];
function _pp0(v) = [v.x,v.y,0];
function _ppp(v) = [v.x,v.y,v.z];
function _ppm(v) = [v.x,v.y,-v.z];
function _pm0(v) = [v.x, -v.y, 0];
function _pmp(v) = [v.x,-v.y,v.z];
function _pmm(v) = [v.x,-v.y,-v.z];

function _m00(v) = [-v.x,0,0];
function _m0p(v) = [-v.x,0,v.z];
function _m0m(v) = [-v.x,0,-v.z];
function _mp0(v) = [-v.x,v.y,0];
function _mpp(v) = [-v.x,v.y,v.z];
function _mpm(v) = [-v.x,v.y,-v.z];
function _mm0(v) = [-v.x,-v.y,0];
function _mmp(v) = [-v.x,-v.y,v.z];
function _mmm(v) = [-v.x,-v.y,-v.z];

/**
   A cube softened by pulling out the middle of each side. Size is a
   3-vector. Softness is how far to pull the sides out. If softness
   exceeds half of any of the dimensions, you get an effect which,
   while interesting, is no longer best described as a soft cube.
   */
module soft_cube_sharp_edges(size, softness) {
     vs = [softness, softness, softness];

     c = [
          [0, 0, 0],
          [0, size.y, 0],
          [size.x, size.y, 0],
          [size.x, 0, 0],
          [0, 0, size.z],
          [0, size.y, size.z],
          [size.x, size.y, size.z],
          [size.x, 0, size.z]];

     top_patch = [
          [c[4]+_000(vs), c[4]+_0p0(vs), c[5]+_0m0(vs), c[5]+_000(vs)],
          [c[4]+_p00(vs), c[4]+_ppp(vs), c[5]+_pmp(vs), c[5]+_p00(vs)],
          [c[7]+_m00(vs), c[7]+_mpp(vs), c[6]+_mmp(vs), c[6]+_m00(vs)],
          [c[7]+_000(vs), c[7]+_0p0(vs), c[6]+_0m0(vs), c[6]+_000(vs)]];

     bottom_patch = [
          [c[0]+_000(vs), c[0]+_0p0(vs), c[1]+_0m0(vs), c[1]+_000(vs)],
          [c[0]+_p00(vs), c[0]+_ppm(vs), c[1]+_pmm(vs), c[1]+_p00(vs)],
          [c[3]+_m00(vs), c[3]+_mpm(vs), c[2]+_mmm(vs), c[2]+_m00(vs)],
          [c[3]+_000(vs), c[3]+_0p0(vs), c[2]+_0m0(vs), c[2]+_000(vs)]];

     back_patch = [
          [c[4]+_000(vs), c[4]+_p00(vs), c[7]+_m00(vs), c[7]+_000(vs)],
          [c[4]+_00m(vs), c[4]+_pmm(vs), c[7]+_mmm(vs), c[7]+_00m(vs)],
          [c[0]+_00p(vs), c[0]+_pmp(vs), c[3]+_mmp(vs), c[3]+_00p(vs)],
          [c[0]+_000(vs), c[0]+_p00(vs), c[3]+_m00(vs), c[3]+_000(vs)]];

     front_patch = [
          [c[5]+_000(vs), c[5]+_p00(vs), c[6]+_m00(vs), c[6]+_000(vs)],
          [c[5]+_00m(vs), c[5]+_ppm(vs), c[6]+_mpm(vs), c[6]+_00m(vs)],
          [c[1]+_00p(vs), c[1]+_ppp(vs), c[2]+_mpp(vs), c[2]+_00p(vs)],
          [c[1]+_000(vs), c[1]+_p00(vs), c[2]+_m00(vs), c[2]+_000(vs)]];

     left_patch = [
          [c[0]+_000(vs), c[0]+_0p0(vs), c[1]+_0m0(vs), c[1]+_000(vs)],
          [c[0]+_00p(vs), c[0]+_mpp(vs), c[1]+_mmp(vs), c[1]+_00p(vs)],
          [c[4]+_00m(vs), c[4]+_mpm(vs), c[5]+_mmm(vs), c[5]+_00m(vs)],
          [c[4]+_000(vs), c[4]+_0p0(vs), c[5]+_0m0(vs), c[5]+_000(vs)]];

     right_patch = [
          [c[3]+_000(vs), c[3]+_0p0(vs), c[2]+_0m0(vs), c[2]+_000(vs)],
          [c[3]+_00p(vs), c[3]+_ppp(vs), c[2]+_pmp(vs), c[2]+_00p(vs)],
          [c[7]+_00m(vs), c[7]+_ppm(vs), c[6]+_pmm(vs), c[6]+_00m(vs)],
          [c[7]+_000(vs), c[7]+_0p0(vs), c[6]+_0m0(vs), c[6]+_000(vs)]];


/* trace_bezier_patches(patches=[top_patch], size=1, showcps=true); */
/* trace_bezier_patches(patches=[bottom_patch], size=1, showcps=true); */
/* trace_bezier_patches(patches=[back_patch], size=1, showcps=true); */
/* trace_bezier_patches(patches=[front_patch], size=1, showcps=true); */
/* trace_bezier_patches(patches=[left_patch], size=1, showcps=true); */
/* trace_bezier_patches(patches=[right_patch], size=1, showcps=true); */
vnf = bezier_surface(patches=[top_patch, bottom_patch,
                                   back_patch, front_patch,
                                   left_patch, right_patch]);
     polyhedron(points=vnf[0], faces=vnf[1]);
}

// elementwise multiplication, such as you would get by multiplying a
// row vector and a column vector together
function elem_mult(a, b) =
     [for(i=[0:len(a)-1]) a[i] * b[i]];

function grid_points(corner_a, corner_b, unit_u, unit_v, su, sv) =
     [let(au = elem_mult(unit_u, (corner_b - corner_a)) / su,
          av = elem_mult(unit_v, (corner_b - corner_a)) / sv)
      for(u=[0:su], v=[0:sv])
           corner_a + au * u + av * v];

module cube_c2_fillet(size, fillet, control, splinesteps=8, center=false) {
     c = [
          [0, 0, 0],
          [0, size.y, 0],
          [size.x, size.y, 0],
          [size.x, 0, 0],
          [0, 0, size.z],
          [0, size.y, size.z],
          [size.x, size.y, size.z],
          [size.x, 0, size.z]];

     // the reason this is a flat array and we do arithmetic to
     // calculate indices of it is so that we can pass it as the
     // points array of a polyhedron.
     inset_points = [
          c[0] + _pp0(fillet), c[0] + _p0p(fillet), c[0] + _0pp(fillet),
          c[1] + _pm0(fillet), c[1] + _p0p(fillet), c[1] + _0mp(fillet),
          c[2] + _mm0(fillet), c[2] + _m0p(fillet), c[2] + _0mp(fillet),
          c[3] + _mp0(fillet), c[3] + _m0p(fillet), c[3] + _0pp(fillet),
          c[4] + _pp0(fillet), c[4] + _p0m(fillet), c[4] + _0pm(fillet),
          c[5] + _pm0(fillet), c[5] + _p0m(fillet), c[5] + _0mm(fillet),
          c[6] + _mm0(fillet), c[6] + _m0m(fillet), c[6] + _0mm(fillet),
          c[7] + _mp0(fillet), c[7] + _m0m(fillet), c[7] + _0pm(fillet)];

     control_offsets = [
          // 0 -> 1    , 1 -> 0       , 0 -> 2       , 2 -> 0       , 1 -> 2       , 2 -> 1
          _0m0(control), _00m(control), _m00(control), _00m(control), _m00(control), _0m0(control),
          _0p0(control), _00m(control), _m00(control), _00m(control), _m00(control), _0p0(control),
          _0p0(control), _00m(control), _p00(control), _00m(control), _p00(control), _0p0(control),
          _0m0(control), _00m(control), _p00(control), _00m(control), _p00(control), _0m0(control),
          _0m0(control), _00p(control), _m00(control), _00p(control), _m00(control), _0m0(control),
          _0p0(control), _00p(control), _m00(control), _00p(control), _m00(control), _0p0(control),
          _0p0(control), _00p(control), _p00(control), _00p(control), _p00(control), _0p0(control),
          _0m0(control), _00p(control), _p00(control), _00p(control), _p00(control), _0m0(control)];

     // control points
     cpts = [for(p=[0:7], off=[0,1,0,2,1,2]) inset_points[p*3+off]] + control_offsets;

     // to make the corners bulbous, change this
     corner_control_points = c;

     // e.g.
     /* corner_control_points = [ */
          /* c[0] + _mmm(fillet - control), */
          /* c[1] + _mpm(fillet - control), */
          /* c[2] + _ppm(fillet - control), */
          /* c[3] + _pmm(fillet - control), */
          /* c[4] + _mmp(fillet - control), */
          /* c[5] + _mpp(fillet - control), */
          /* c[6] + _ppp(fillet - control), */
          /* c[7] + _pmp(fillet - control)]; */

     ss = splinesteps;

     flat_points = concat(
          grid_points(inset_points[0*3+0], inset_points[2*3+0], [1,0,0], [0,1,0], ss, ss),
          grid_points(inset_points[4*3+0], inset_points[6*3+0], [1,0,0], [0,1,0], ss, ss),
          grid_points(inset_points[0*3+2], inset_points[5*3+2], [0,1,0], [0,0,1], ss, ss),
          grid_points(inset_points[3*3+2], inset_points[6*3+2], [0,1,0], [0,0,1], ss, ss),
          grid_points(inset_points[1*3+1], inset_points[6*3+1], [1,0,0], [0,0,1], ss, ss),
          grid_points(inset_points[0*3+1], inset_points[7*3+1], [1,0,0], [0,0,1], ss, ss)
          );

     flat_indices = concat(
          face_off(x_grid_faces_a(ss, ss), 0*(ss+1)*(ss+1)),
          face_off(x_grid_faces_b(ss, ss), 0*(ss+1)*(ss+1)),
          face_off(minusx_grid_faces_a(ss, ss), 1*(ss+1)*(ss+1)),
          face_off(minusx_grid_faces_b(ss, ss), 1*(ss+1)*(ss+1)),
          face_off(x_grid_faces_a(ss, ss), 2*(ss+1)*(ss+1)),
          face_off(x_grid_faces_b(ss, ss), 2*(ss+1)*(ss+1)),
          face_off(minusx_grid_faces_a(ss, ss), 3*(ss+1)*(ss+1)),
          face_off(minusx_grid_faces_b(ss, ss), 3*(ss+1)*(ss+1)),
          face_off(x_grid_faces_a(ss, ss), 4*(ss+1)*(ss+1)),
          face_off(x_grid_faces_b(ss, ss), 4*(ss+1)*(ss+1)),
          face_off(minusx_grid_faces_a(ss, ss), 5*(ss+1)*(ss+1)),
          face_off(minusx_grid_faces_b(ss, ss), 5*(ss+1)*(ss+1))
          );

     edge_patches = [
                                [
               [inset_points[0*3+0], cpts[0*6+2], cpts[0*6+3], inset_points[0*3+2]],
           [inset_points[1*3+0], cpts[1*6+2], cpts[1*6+3], inset_points[1*3+2]],
               ],
          [
               [inset_points[1*3+0], cpts[1*6+0], cpts[1*6+1], inset_points[1*3+1]],
           [inset_points[2*3+0], cpts[2*6+0], cpts[2*6+1], inset_points[2*3+1]],
               ],
          [
               [inset_points[2*3+0], cpts[2*6+2], cpts[2*6+3], inset_points[2*3+2]],
           [inset_points[3*3+0], cpts[3*6+2], cpts[3*6+3], inset_points[3*3+2]],
               ],
          [
               [inset_points[3*3+0], cpts[3*6+0], cpts[3*6+1], inset_points[3*3+1]],
           [inset_points[0*3+0], cpts[0*6+0], cpts[0*6+1], inset_points[0*3+1]],
               ],

          [[inset_points[4*3+2], cpts[4*6+3], cpts[4*6+2], inset_points[4*3+0]],
           [inset_points[5*3+2], cpts[5*6+3], cpts[5*6+2], inset_points[5*3+0]]],
          [[inset_points[5*3+1], cpts[5*6+1], cpts[5*6+0], inset_points[5*3+0]],
           [inset_points[6*3+1], cpts[6*6+1], cpts[6*6+0], inset_points[6*3+0]]],
          [[inset_points[6*3+2], cpts[6*6+3], cpts[6*6+2], inset_points[6*3+0]],
           [inset_points[7*3+2], cpts[7*6+3], cpts[7*6+2], inset_points[7*3+0]]],
          [[inset_points[7*3+1], cpts[7*6+1], cpts[7*6+0], inset_points[7*3+0]],
           [inset_points[4*3+1], cpts[4*6+1], cpts[4*6+0], inset_points[4*3+0]]],

          [[inset_points[0*3+2], cpts[0*6+5], cpts[0*6+4], inset_points[0*3+1]],
           [inset_points[4*3+2], cpts[4*6+5], cpts[4*6+4], inset_points[4*3+1]]],
          [[inset_points[1*3+1], cpts[1*6+4], cpts[1*6+5], inset_points[1*3+2]],
           [inset_points[5*3+1], cpts[5*6+4], cpts[5*6+5], inset_points[5*3+2]]],
          [[inset_points[2*3+2], cpts[2*6+5], cpts[2*6+4], inset_points[2*3+1]],
           [inset_points[6*3+2], cpts[6*6+5], cpts[6*6+4], inset_points[6*3+1]]],
          [[inset_points[3*3+1], cpts[3*6+4], cpts[3*6+5], inset_points[3*3+2]],
           [inset_points[7*3+1], cpts[7*6+4], cpts[7*6+5], inset_points[7*3+2]]]];

     corner_tris = [

          [[inset_points[0*3+0], cpts[0*6+2], cpts[0*6+3], inset_points[0*3+2]],
           [cpts[0*6+0], corner_control_points[0], cpts[0*6+5]],
           [cpts[0*6+1], cpts[0*6+4]],
           [inset_points[0*3+1]]],

          [[inset_points[1*3+2], cpts[1*6+3], cpts[1*6+2], inset_points[1*3+0]],
           [cpts[1*6+5], corner_control_points[1], cpts[1*6+0]],
           [cpts[1*6+4], cpts[1*6+1]],
           [inset_points[1*3+1]]],

          [[inset_points[2*3+0], cpts[2*6+2], cpts[2*6+3], inset_points[2*3+2]],
           [cpts[2*6+0], corner_control_points[2], cpts[2*6+5]],
           [cpts[2*6+1], cpts[2*6+4]],
           [inset_points[2*3+1]]],

          [[inset_points[3*3+2], cpts[3*6+3], cpts[3*6+2], inset_points[3*3+0]],
           [cpts[3*6+5], corner_control_points[3], cpts[3*6+0]],
           [cpts[3*6+4], cpts[3*6+1]],
           [inset_points[3*3+1]]],

          [[inset_points[4*3+2], cpts[4*6+3], cpts[4*6+2], inset_points[4*3+0]],
           [cpts[4*6+5], corner_control_points[4], cpts[4*6+0]],
           [cpts[4*6+4], cpts[4*6+1]],
           [inset_points[4*3+1]]],

          [[inset_points[5*3+0], cpts[5*6+2], cpts[5*6+3], inset_points[5*3+2]],
           [cpts[5*6+0], corner_control_points[5], cpts[5*6+5]],
           [cpts[5*6+1], cpts[5*6+4]],
           [inset_points[5*3+1]]],

          [[inset_points[6*3+2], cpts[6*6+3], cpts[6*6+2], inset_points[6*3+0]],
           [cpts[6*6+5], corner_control_points[6], cpts[6*6+0]],
           [cpts[6*6+4], cpts[6*6+1]],
           [inset_points[6*3+1]]],

          [[inset_points[7*3+0], cpts[7*6+2], cpts[7*6+3], inset_points[7*3+2]],
           [cpts[7*6+0], corner_control_points[7], cpts[7*6+5]],
           [cpts[7*6+1], cpts[7*6+4]],
           [inset_points[7*3+1]]],

          ];

     /* trace_bezier_patches(patches=edge_patches, */
                          /* tris=corner_tris, */
                          /* splinesteps=splinesteps, */
                          /* showcps=true); */
     /* polyhedron(flat_points, flat_indices); */

     centering = center ? (-1/2 * size) : [0,0,0];
     translate(centering)
     bezier_polyhedron(patches=edge_patches,
                       tris=corner_tris,
                            splinesteps=splinesteps,
                            vertices=flat_points, faces=flat_indices);
}
